﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ZOO.Models;

namespace ZOO.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<TicketPurchase> TicketPurchases { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Membership> Memberships { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<OtherPurchase> OtherPurchases { get; set; }
        public DbSet<GiftShop> GiftShops { get; set; }
        public DbSet<Animal> Animals { get; set; }
        public DbSet<Attraction> Attractions { get; set; }
        public DbSet<Worker> Workers { get; set; }
        public DbSet<AnimalWorker> AnimalWokers { get; set; }
        public DbSet<AttractionTicket> AttractionTickets { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Animal>()
                .HasOne<Attraction>(s => s.Attraction)
                .WithMany(g => g.Animal)
                .OnDelete(DeleteBehavior.SetNull);
        }

    }
}
