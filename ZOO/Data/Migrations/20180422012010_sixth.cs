﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ZOO.Data.Migrations
{
    public partial class sixth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TicketPurchases_CustomerID",
                table: "TicketPurchases");

            migrationBuilder.DropIndex(
                name: "IX_OtherPurchases_CustomerID",
                table: "OtherPurchases");

            migrationBuilder.AddColumn<decimal>(
                name: "Tax",
                table: "TicketPurchases",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateIndex(
                name: "IX_TicketPurchases_CustomerID",
                table: "TicketPurchases",
                column: "CustomerID");

            migrationBuilder.CreateIndex(
                name: "IX_OtherPurchases_CustomerID",
                table: "OtherPurchases",
                column: "CustomerID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TicketPurchases_CustomerID",
                table: "TicketPurchases");

            migrationBuilder.DropIndex(
                name: "IX_OtherPurchases_CustomerID",
                table: "OtherPurchases");

            migrationBuilder.DropColumn(
                name: "Tax",
                table: "TicketPurchases");

            migrationBuilder.CreateIndex(
                name: "IX_TicketPurchases_CustomerID",
                table: "TicketPurchases",
                column: "CustomerID",
                unique: true,
                filter: "[CustomerID] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_OtherPurchases_CustomerID",
                table: "OtherPurchases",
                column: "CustomerID",
                unique: true,
                filter: "[CustomerID] IS NOT NULL");
        }
    }
}
