﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ZOO.Models
{
    public class Animal
    {
        public int ID { get; set; }

        public string Gender { get; set; }
        public string Name { get; set; }
        public string SpeciesType { get; set; }
        public string Description { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DOB { get; set; }
        public bool Vaccinated { get; set; }

        public byte[] AvatarImage { get; set; }
        public DateTime? AddedOn { get; set; }
        public string UpdatedBy { get; set; }

        public int? AttractionID { get; set; }

        public Attraction Attraction { get; set; }
        public List<AnimalWorker> AnimalWorker { get; set; }
        

    }
}
