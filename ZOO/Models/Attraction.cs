﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ZOO.Models
{
    public class Attraction
    {
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        [DataType(DataType.Date)]
        public DateTime? AddedOn { get; set; }
        public string UpdatedBy { get; set; }
        public byte[] AvatarImage { get; set; }

        public List<AttractionTicket> Ticket { get; set; }
        public Worker Worker { get; set; }
        public List<Animal> Animal { get; set; }

    }
}
