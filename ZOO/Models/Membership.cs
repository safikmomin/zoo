﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ZOO.Models
{
    public class Membership
    {
        public int ID { get; set; }

        public decimal Price { get; set; }
        public int DateRange { get; set; }
        public bool Active { get; set; }
        [DataType(DataType.Date)]
        public DateTime? AddedOn { get; set; }
        public string UpdatedBy { get; set; }

        public List<TicketPurchase> TicketPurchase { get; set; }

    }
}
