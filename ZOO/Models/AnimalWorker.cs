﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZOO.Models
{
    public class AnimalWorker
    {
        public int ID { get; set; }

        public int AnimalID { get; set; }
        public int WorkerID { get; set; }

        public Animal Animal { get; set; }
        public Worker Worker { get; set; }
    }
}
