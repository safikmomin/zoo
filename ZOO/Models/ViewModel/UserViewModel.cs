﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZOO.Models.ViewModel
{
    public class UserViewModel
    {
        public string UserEmail { get; set; }

        public List<IdentityUserRole<string>> Roles { get; set; }
    }
}
