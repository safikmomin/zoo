﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ZOO.Models
{
    public class OtherPurchase
    {
        public int ID { get; set; }

        public decimal? Amount { get; set; }
        public DateTime Date { get; set; }
        public int? Quantity { get; set; }
        public string PaymentType { get; set; }
        [DataType(DataType.Date)]
        public DateTime? AddedOn { get; set; }
        public string UpdatedBy { get; set; }

        public int? CustomerID { get; set; }
        public int? GiftShopID { get; set; }

        public Customer Customer { get; set; }
        public GiftShop GiftShop { get; set; }

    }
}
