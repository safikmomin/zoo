﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ZOO.Models
{
    public class TicketPurchase
    {
        public int ID { get; set; }

        public decimal Amount { get; set; }
        public decimal Tax { get; set; }
        public decimal? PaymentType { get; set; }
        public int Quantity { get; set; }
        [DataType(DataType.Date)]
        public DateTime? Date { get; set; }
        public string UpdatedBy { get; set; }

        public int? TicketID { get; set; }
        public int? CustomerID { get; set; }
        public int? MembershipID { get; set; }

        public Ticket Ticket { get; set; }
        public Customer Customer { get; set; }
        public Membership Membership { get; set; }

    }
}
