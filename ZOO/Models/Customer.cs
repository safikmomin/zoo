﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ZOO.Models
{
    public class Customer
    {
        public int ID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        [DataType(DataType.Date)]
        public DateTime? AddedOn { get; set; }
        public string UpdatedBy { get; set; }

        public string ApplicationUserID { get; set; }
        
        public List<OtherPurchase> OtherPurchase { get; set; }
        public List<TicketPurchase> TicketPurchase { get; set; }

    }
}
