﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ZOO.Models
{
    public class AttractionTicket
    {
        public int ID { get; set; }

        [ForeignKey("Ticket")]
        public int? TicketID { get; set; }
        [ForeignKey("Attraction")]
        public int? AttractionId { get; set; }

        public Ticket Ticket { get; set; }
        public Attraction Attraction { get; set; }

    }
}
