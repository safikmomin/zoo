﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ZOO.Models
{
    public class GiftShop
    {
        public int ID { get; set; }

        public string Name { get; set; }
        public string Location { get; set; }
        [DataType(DataType.Date)]
        public DateTime? AddedOn { get; set; }
        public string UpdatedBy { get; set; }

        public List<OtherPurchase> OtherPurchase { get; set; }
        public List<Worker> Worker { get; set; }

    }
}
