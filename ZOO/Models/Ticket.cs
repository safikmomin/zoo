﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ZOO.Models
{
    public class Ticket
    {
        [Key]
        public int ID { get; set; }

        public string Type { get; set; }
        public decimal? Price { get; set; }
        [DataType(DataType.Date)]
        public DateTime? AddedOn { get; set; }
        public string UpdatedBy { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        public List<AttractionTicket> Attraction { get; set; }
        public List<TicketPurchase> Purchase { get; set; }
    }
}
