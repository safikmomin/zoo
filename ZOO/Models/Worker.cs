﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ZOO.Models
{
    public class Worker
    {
        public int ID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateJoined { get; set; }
        [DataType(DataType.Date)]
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        [DataType(DataType.Date)]
        public DateTime? AddedOn { get; set; }
        public string UpdatedBy { get; set; }

        public string ApplicationUserID { get; set; }

        public int? AttractionID { get; set; }
        public int? GiftShopID { get; set; }

        public Attraction Attraction { get; set; }
        public GiftShop GiftShop { get; set; }

    }
}
