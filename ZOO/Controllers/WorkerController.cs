﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ZOO.Data;
using ZOO.Models;
using ZOO.Models.ViewModel;

namespace ZOO.Controllers
{
    [Authorize(Roles ="Admin")]
    public class WorkerController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public WorkerController(UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Worker
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Workers.Include(w => w.Attraction).Include(w => w.GiftShop);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Worker/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var worker = await _context.Workers
                .Include(w => w.Attraction)
                .Include(w => w.GiftShop)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (worker == null)
            {
                return NotFound();
            }

            return View(worker);
        }

        // GET: Worker/Create
        public IActionResult Create()
        {
            ViewData["AttractionID"] = new SelectList(_context.Attractions, "ID", "Name");
            return View();
        }

        // POST: Worker/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(WorkerViewModel worker)
        {
            var users = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            Worker work = new Worker()
            {
                FirstName = worker.FirstName,
                LastName = worker.LastName,
                PhoneNumber = worker.PhoneNumber,
                DateJoined = worker.DateJoined,
                DOB = worker.DOB,
                Gender = worker.Gender,
                AddedOn = DateTime.Now,
                AttractionID = worker.AttractionID,
                ApplicationUserID = users
            };

            var user = new ApplicationUser { UserName = worker.Email, Email = worker.Email };
            var result = await _userManager.CreateAsync(user, worker.Password);

            if(result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "Worker");
            }

            if (ModelState.IsValid)
            {
                _context.Add(work);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AttractionID"] = new SelectList(_context.Attractions, "ID", "ID", worker.AttractionID);
            ViewData["GiftShopID"] = new SelectList(_context.GiftShops, "ID", "ID", worker.GiftShopID);
            return View(worker);
        }

        // GET: Worker/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var worker = await _context.Workers.SingleOrDefaultAsync(m => m.ID == id);
            if (worker == null)
            {
                return NotFound();
            }
            ViewData["AttractionID"] = new SelectList(_context.Attractions, "ID", "ID", worker.AttractionID);
            ViewData["GiftShopID"] = new SelectList(_context.GiftShops, "ID", "ID", worker.GiftShopID);
            return View(worker);
        }

        // POST: Worker/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Worker worker)
        {
            if (id != worker.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(worker);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WorkerExists(worker.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AttractionID"] = new SelectList(_context.Attractions, "ID", "ID", worker.AttractionID);
            ViewData["GiftShopID"] = new SelectList(_context.GiftShops, "ID", "ID", worker.GiftShopID);
            return View(worker);
        }

        // GET: Worker/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var worker = await _context.Workers
                .Include(w => w.Attraction)
                .Include(w => w.GiftShop)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (worker == null)
            {
                return NotFound();
            }

            return View(worker);
        }

        // POST: Worker/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var worker = await _context.Workers.SingleOrDefaultAsync(m => m.ID == id);
            _context.Workers.Remove(worker);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WorkerExists(int id)
        {
            return _context.Workers.Any(e => e.ID == id);
        }
    }
}
