﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ZOO.Data;
using ZOO.Models;
using ZOO.Models.ViewModel;

namespace ZOO.Controllers
{
    public class TicketsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TicketsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Tickets
        public async Task<IActionResult> Index()
        {
            return View(await _context.Tickets.ToListAsync());
        }

        [Authorize]
        // GET: Tickets/Details/5
        public async Task<IActionResult> Details(/*int? id*/)
        {
            //if (id == null)
            //{
            //    return NotFound();
            //}

            //var ticket = await _context.Tickets
            //    .SingleOrDefaultAsync(m => m.ID == id);
            //if (ticket == null)
            //{
            //    return NotFound();
            //}
            if (User.IsInRole("Admin"))
            {
                var user = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                var customer = await _context.Customers.Where(a => a.ApplicationUserID == user).ToListAsync();
                if (customer.Count < 1)
                {
                    Customer cus = new Customer()
                    {
                        FirstName = "Safik",
                        LastName = "Momin",
                        PhoneNumber = "8328292659",
                        ApplicationUserID = user
                    };
                    _context.Add(cus);
                    await _context.SaveChangesAsync();
                }
            }


            ViewBag.ListCount = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
            ViewBag.Ticket = await _context.Tickets.ToListAsync();
            return View();
            //return View(ticket);
        }

        public async Task<IActionResult> ThankYou(List<TicketsViewModel> model)
        {
            var user = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var customer = await _context.Customers.Where(a => a.ApplicationUserID == user).FirstAsync();
            List<TicketPurchase> AllPurchases = new List<TicketPurchase>();
            foreach (var m in model)
            {
                if (m.Count.GetValueOrDefault() > 0)
                {
                    var tix = await _context.Tickets.Where(a => a.ID == m.ID).FirstAsync();
                    decimal price = m.Price.GetValueOrDefault(0M) * m.Count.GetValueOrDefault();
                    decimal tax = price * Decimal.Parse(0.0825.ToString());
                    TicketPurchase purchase = new TicketPurchase()
                    {
                        Amount = price + tax,
                        Tax = tax,
                        PaymentType = 0,
                        Quantity = m.Count.GetValueOrDefault(),
                        Date = DateTime.Now,
                        UpdatedBy = user,
                        TicketID = m.ID,
                        CustomerID = customer.ID,
                        Customer = customer,
                        Ticket = tix
                    };
                    _context.Add(purchase);
                    await _context.SaveChangesAsync();
                    AllPurchases.Add(purchase);
                }
            }
            return View(AllPurchases);
        }

        [Authorize]
        public async Task<IActionResult> ThankYouMember()
        {
            var user = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var customer = await _context.Customers.Where(a => a.ApplicationUserID == user).FirstAsync();
            List<TicketPurchase> AllPurchases = new List<TicketPurchase>();

            var mem = await _context.Memberships.FirstAsync();
            decimal price = mem.Price;
            decimal tax = price * Decimal.Parse(0.0825.ToString());
            TicketPurchase purchase = new TicketPurchase()
            {
                Amount = price + tax,
                Tax = tax,
                PaymentType = 0,
                Quantity = 1,
                Date = DateTime.Now,
                UpdatedBy = user,
                MembershipID = mem.ID,
                CustomerID = customer.ID,
                Customer = customer,
                Membership = mem
            };
            _context.Add(purchase);
            await _context.SaveChangesAsync();
            AllPurchases.Add(purchase);

            return View("ThankYou", AllPurchases);
        }

        // GET: Tickets/Create
        [Authorize(Roles = "Admin, Worker")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Worker")]
        public async Task<IActionResult> Create([Bind("ID,Type,Price,Date")] Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                ticket.Date = DateTime.Now;
                ticket.UpdatedBy = User.Identity.Name;
                _context.Add(ticket);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(ticket);
        }

        // GET: Tickets/Edit/5
        [Authorize(Roles = "Admin, Worker")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _context.Tickets.SingleOrDefaultAsync(m => m.ID == id);
            if (ticket == null)
            {
                return NotFound();
            }
            return View(ticket);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Worker")]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Type,Price,Date")] Ticket ticket)
        {
            if (id != ticket.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    ticket.AddedOn = DateTime.Now;
                    ticket.UpdatedBy = User.Identity.Name;
                    _context.Update(ticket);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TicketExists(ticket.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(ticket);
        }

        // GET: Tickets/Delete/5
        [Authorize(Roles = "Admin, Worker")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _context.Tickets
                .SingleOrDefaultAsync(m => m.ID == id);
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Worker")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ticket = await _context.Tickets.SingleOrDefaultAsync(m => m.ID == id);
            _context.Tickets.Remove(ticket);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TicketExists(int id)
        {
            return _context.Tickets.Any(e => e.ID == id);
        }
    }
}
