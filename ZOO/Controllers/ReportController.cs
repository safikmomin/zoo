﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ZOO.Data;

namespace ZOO.Controllers
{
    public class ReportController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ReportController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Animal(DateTime fromTime, DateTime toTime)
        {
            int x = 0;

            if (int.TryParse(fromTime.Year.ToString(), out x) && x < 2)
            {
                fromTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            }
            else
            {
                fromTime = new DateTime(fromTime.Year, fromTime.Month, fromTime.Day, 0, 0, 0);
            }
            if (int.TryParse(toTime.Year.ToString(), out x) && x < 2)
            {
                toTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            }
            else
            {
                toTime = new DateTime(toTime.Year, toTime.Month, toTime.Day, 23, 59, 59);
            }

            ViewBag.fromTime = fromTime;
            ViewBag.toTime = toTime;

            return View(_context.Animals.Where(a => a.AddedOn >= fromTime && a.AddedOn <= toTime).ToList());
        }

        public IActionResult Ticket(DateTime fromTime, DateTime toTime)
        {
            int x = 0;

            if (int.TryParse(fromTime.Year.ToString(), out x) && x < 2)
            {
                fromTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            }
            else
            {
                fromTime = new DateTime(fromTime.Year, fromTime.Month, fromTime.Day, 0, 0, 0);
            }
            if (int.TryParse(toTime.Year.ToString(), out x) && x < 2)
            {
                toTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            }
            else
            {
                toTime = new DateTime(toTime.Year, toTime.Month, toTime.Day, 23, 59, 59);
            }

            return View(_context.TicketPurchases.Where(t => t.Date >= fromTime && t.Date <= toTime).ToList());
        }

        public IActionResult Item(DateTime fromTime, DateTime toTime)
        {
            int x = 0;

            if (int.TryParse(fromTime.Year.ToString(), out x) && x < 2)
            {
                fromTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            }
            else
            {
                fromTime = new DateTime(fromTime.Year, fromTime.Month, fromTime.Day, 0, 0, 0);
            }
            if (int.TryParse(toTime.Year.ToString(), out x) && x < 2)
            {
                toTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            }
            else
            {
                toTime = new DateTime(toTime.Year, toTime.Month, toTime.Day, 23, 59, 59);
            }

            return View(_context.OtherPurchases.Where(p => p.Date >= fromTime && p.Date <= toTime));
        }
    }
}