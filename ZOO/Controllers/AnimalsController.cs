﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ZOO.Data;
using ZOO.Models;
using ZOO.Models.ViewModel;

namespace ZOO.Controllers
{
    public class AnimalsController : Controller
    {
        private readonly ApplicationDbContext _context;
        

        public AnimalsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Animals
        public async Task<IActionResult> Index(string Search)
        {

            //var sometext = _context.Database.SqlQuery<string>("SELECT * From Animals").ToArray();
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "SELECT * From dbo.Animals";
                _context.Database.OpenConnection();
                using (SqlDataReader result = (SqlDataReader)command.ExecuteReader())
                {
                    DataTable schemaTable = result.GetSchemaTable();
                    foreach (DataRow row in schemaTable.Rows)
                    {
                        foreach (DataColumn column in schemaTable.Columns)
                        {
                            var something = String.Format("{0} = {1}", column.ColumnName, row[column]);
                        }
                    }
                    // do something with result
                }
            }
            
            if (String.IsNullOrWhiteSpace(Search))
            {
                var applicationDbContext = _context.Animals.Include(a => a.Attraction);
                return View(await applicationDbContext.ToListAsync());
            }
            else
            {
                var applicationDbContext = _context.Animals.Where(a => 
                    a.Name.Contains(Search) || a.SpeciesType.Contains(Search))
                    .Include(a => a.Attraction);
                return View(await applicationDbContext.ToListAsync());
            }
            
        }

        // GET: Animals/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var animal = await _context.Animals
                .Include(a => a.Attraction)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (animal == null)
            {
                return NotFound();
            }

            return View(animal);
        }

        // GET: Animals/Create
        [Authorize(Roles ="Admin, Worker")]
        public IActionResult Create()
        {
            ViewData["AttractionID"] = new SelectList(_context.Attractions, "ID", "Name");
            ViewData["Gender"] = new SelectList(new[]
                                {
                                    new {Gender="Male"},
                                    new{Gender="Female"},
                                }, "Gender", "Gender");
            return View();
        }

        // POST: Animals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Worker")]
        //[Bind("ID,Gender,Name,SpeciesType,DOB,Vaccinated,AttractionID","AvatarImage")]
        public async Task<IActionResult> Create( Animal animal, IFormFile files)
        {
            if (ModelState.IsValid)
            {
                if (files != null)
                {

                    using (var memoryStream = new MemoryStream())
                    {
                        await files.CopyToAsync(memoryStream);
                        animal.AvatarImage = memoryStream.ToArray();
                    }
                }
                animal.AddedOn = DateTime.Now;
                animal.UpdatedBy = User.Identity.Name;
                _context.Add(animal);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AttractionID"] = new SelectList(_context.Attractions, "ID", "Name", animal.AttractionID);
            ViewData["Gender"] = new SelectList(new[]
                                {
                                    new {Gender="Male"},
                                    new{Gender="Female"},
                                }, "Gender", "Gender");
            return View(animal);
        }

        // GET: Animals/Edit/5
        [Authorize(Roles = "Admin, Worker")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var animal = await _context.Animals.SingleOrDefaultAsync(m => m.ID == id);
            if (animal == null)
            {
                return NotFound();
            }
            ViewData["Gender"] = new SelectList(new[]
                                {
                                    new {Gender="Male"},
                                    new{Gender="Female"},
                                }, "Gender", "Gender");
            ViewData["AttractionID"] = new SelectList(_context.Attractions, "ID", "Name", animal.AttractionID);
            return View(animal);
        }

        // POST: Animals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Worker")]
        public async Task<IActionResult> Edit(int id, Animal animal, IFormFile files)
        {
            if (id != animal.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (files != null)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        await files.CopyToAsync(memoryStream);
                        animal.AvatarImage = memoryStream.ToArray();
                    }
                }
                
                try
                {
                    if(animal.AddedOn == null)
                    {
                        animal.AddedOn = DateTime.Now;
                    }
                    animal.UpdatedBy = User.Identity.Name;
                    _context.Update(animal);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AnimalExists(animal.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Gender"] = new SelectList(new[]
                                {
                                    new {Gender="Male"},
                                    new{Gender="Female"},
                                }, "Gender", "Gender");
            ViewData["AttractionID"] = new SelectList(_context.Attractions, "ID", "Name", animal.AttractionID);
            return View(animal);
        }

        // GET: Animals/Delete/5
        [Authorize(Roles = "Admin, Worker")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var animal = await _context.Animals
                .Include(a => a.Attraction)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (animal == null)
            {
                return NotFound();
            }

            return View(animal);
        }

        // POST: Animals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Worker")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var animal = await _context.Animals.SingleOrDefaultAsync(m => m.ID == id);
            _context.Animals.Remove(animal);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AnimalExists(int id)
        {
            return _context.Animals.Any(e => e.ID == id);
        }
    }
}
